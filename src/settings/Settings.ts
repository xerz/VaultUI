import { StorageType } from "./storage/StorageType";

export class Settings {
  constructor() {
    this.storage = localStorage;
  }

  private storage: StorageType;

  get apiURL(): string | null {
    const apiurl = this.storage.getItem("apiURL") || "";
    return apiurl.length > 0 ? apiurl : null;
  }
  set apiURL(value: string) {
    this.storage.setItem("apiURL", value);
  }

  get token(): string | null {
    const tok = this.storage.getItem("token") || "";
    return tok.length > 0 ? tok : null;
  }
  set token(value: string) {
    this.storage.setItem("token", value);
  }

  get pageDirection(): string {
    return this.storage.getItem("pageDirection") || "ltr";
  }
  set pageDirection(value: string) {
    this.storage.setItem("pageDirection", value);
  }

  get language(): string {
    return this.storage.getItem("language") || "";
  }
  set language(value: string) {
    this.storage.setItem("language", value);
  }
}
