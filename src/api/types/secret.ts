export type SecretMetadataType = {
  versions: Record<string, unknown>;
};
