import { API } from "./api/API";
import { Settings } from "./settings/Settings";
import { route } from "preact-router";
import ClipboardJS from "clipboard";
import UIkit from "uikit";
import i18next from "i18next";

async function prePageChecksReal(api: API, settings: Settings) {
  if (settings.language.length == 0) {
    route("/set_language", true);
    throw new Error("Language Not Set");
  }

  if (!settings.apiURL) {
    route("/set_vault_url", true);
    throw new Error("Vault URL Not Set");
  }

  const sealStatus = await api.getSealStatus();
  if (sealStatus.sealed) {
    route("/unseal", true);
    throw new Error("Vault Sealed");
  }

  try {
    await api.lookupSelf();
  } catch (e) {
    route("/login", true);
    throw e;
  }
}

export async function prePageChecks(api: API, settings: Settings): Promise<boolean> {
  try {
    await prePageChecksReal(api, settings);
  } catch (e) {
    console.log("OHNO", e);
    return false;
  }
  return true;
}

export function addClipboardNotifications(clipboard: ClipboardJS, timeout = 1000): void {
  clipboard.on("success", () => {
    UIkit.notification(i18next.t("notification_copy_success"), {
      status: "success",
      timeout: timeout,
    });
  });
  clipboard.on("error", function (e: Error) {
    UIkit.notification(
      i18next.t("notification_copy_error", {
        error: e.message,
      }),
      {
        status: "danger",
        timeout: timeout,
      },
    );
  });
}

export function setErrorText(text: string): void {
  const errorTextElement = document.querySelector("#errorText");
  if (errorTextElement) {
    /* eslint-disable @typescript-eslint/no-unnecessary-type-assertion */
    const p = document.querySelector("#errorText") as HTMLParagraphElement;
    p.innerText = `Error: ${text}`;
    /* eslint-enable @typescript-eslint/no-unnecessary-type-assertion */
  }
  UIkit.notification({
    message: `Error: ${text}`,
    status: "danger",
    pos: "top-center",
    timeout: 2000,
  });
}

export function notImplemented(): void {
  setErrorText(i18next.t("not_implemented"));
}
