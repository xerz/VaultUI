import { API } from "../api/API";
import { Settings } from "../settings/Settings";

export type DefaultPageProps = {
  settings: Settings;
  api: API;
  matches?: { [key: string]: string };
};
