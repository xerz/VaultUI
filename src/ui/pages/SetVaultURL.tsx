import { Component } from "preact";
import { DefaultPageProps } from "../../types/DefaultPageProps";
import { Form } from "../elements/Form";
import { Margin } from "../elements/Margin";
import { PageTitle } from "../elements/PageTitle";
import { route } from "preact-router";

export class SetVaultURL extends Component<DefaultPageProps> {
  render() {
    return (
      <>
        <PageTitle title="Set Vault URL" />
        <Form onSubmit={(data) => this.onSubmit(data)}>
          <Margin>
            <input
              class="uk-input uk-form-width-medium"
              name="vaultURL"
              type="text"
              placeholder="Vault URL"
              required
            />
          </Margin>
          <p id="errorText" class="uk-text-danger" />
          <Margin>
            <button class="uk-button uk-button-primary" type="submit">
              Set
            </button>
          </Margin>
        </Form>
      </>
    );
  }

  async onSubmit(data: FormData): Promise<void> {
    // TODO: check if vault is actually working here.
    this.props.settings.apiURL = data.get("vaultURL") as string;
    route("/");
  }
}
