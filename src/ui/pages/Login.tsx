import { Component, JSX } from "preact";
import { DefaultPageProps } from "../../types/DefaultPageProps";
import { Form } from "../elements/Form";
import { Margin } from "../elements/Margin";
import { MarginInline } from "../elements/MarginInline";
import { PageTitle } from "../elements/PageTitle";
import { route } from "preact-router";
import { setErrorText } from "../../pageUtils";
import i18next from "i18next";

export class TokenLoginForm extends Component<DefaultPageProps> {
  constructor() {
    super();
  }

  render(): JSX.Element {
    return (
      <Form onSubmit={(data) => this.onSubmit(data)}>
        <Margin>
          <input
            class="uk-input uk-form-width-medium"
            id="tokenInput"
            name="token"
            type="password"
            placeholder={i18next.t("log_in_token_input")}
            required
          />
        </Margin>
        <MarginInline>
          <button class="uk-button uk-button-primary" type="submit">
            {i18next.t("log_in_btn")}
          </button>
        </MarginInline>
      </Form>
    );
  }

  async onSubmit(data: FormData): Promise<void> {
    const token = data.get("token");
    this.props.settings.token = token as string;

    try {
      await this.props.api.lookupSelf();
      route("/");
    } catch (e: unknown) {
      const error = e as Error;
      document.querySelector("#tokenInput").classList.add("uk-form-danger");
      if (error.message == "permission denied") {
        setErrorText(i18next.t("log_in_token_login_error"));
      } else {
        setErrorText(error.message);
      }
    }
  }
}

export class UsernameLoginForm extends Component<DefaultPageProps> {
  constructor() {
    super();
  }

  render(): JSX.Element {
    return (
      <Form onSubmit={(data) => this.onSubmit(data)}>
        <Margin>
          <input
            class="uk-input uk-form-width-medium"
            id="usernameInput"
            name="username"
            type="text"
            placeholder={i18next.t("log_in_username_input")}
            required
          />
        </Margin>
        <Margin>
          <input
            class="uk-input uk-form-width-medium"
            id="passwordInput"
            name="password"
            type="password"
            placeholder={i18next.t("log_in_password_input")}
            required
          />
        </Margin>
        <MarginInline>
          <button class="uk-button uk-button-primary" type="submit">
            {i18next.t("log_in_btn")}
          </button>
        </MarginInline>
      </Form>
    );
  }

  async onSubmit(data: FormData): Promise<void> {
    try {
      const res = await this.props.api.usernameLogin(
        data.get("username") as string,
        data.get("password") as string,
      );
      this.props.settings.token = res;
      route("/");
    } catch (e: unknown) {
      const error = e as Error;
      document.querySelector("#usernameInput").classList.add("uk-form-danger");
      document.querySelector("#passwordInput").classList.add("uk-form-danger");
      setErrorText(error.message);
    }
  }
}

export class Login extends Component<DefaultPageProps> {
  render(): JSX.Element {
    return (
      <>
        <PageTitle title={i18next.t("log_in_title")} />
        <div>
          <ul class="uk-subnav uk-subnav-pill" uk-switcher=".switcher-container">
            <li>
              <a>{i18next.t("log_in_with_token")}</a>
            </li>
            <li>
              <a>{i18next.t("log_in_with_username")}</a>
            </li>
          </ul>
          <p id="errorText" class="uk-text-danger" />
          <ul class="uk-switcher uk-margin switcher-container">
            <li>
              <TokenLoginForm settings={this.props.settings} api={this.props.api} />
            </li>
            <li>
              <UsernameLoginForm settings={this.props.settings} api={this.props.api} />
            </li>
          </ul>
        </div>
      </>
    );
  }
}
