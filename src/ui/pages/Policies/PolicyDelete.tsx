import { Component } from "preact";
import { DefaultPageProps } from "../../../types/DefaultPageProps";
import { PageTitle } from "../../elements/PageTitle";
import { route } from "preact-router";
import { setErrorText } from "../../../pageUtils";
import i18next from "i18next";

export class PolicyDelete extends Component<DefaultPageProps> {
  render() {
    const policyName = this.props.matches["policyName"];
    return (
      <>
        <PageTitle title={i18next.t("policy_delete_title", { policy: policyName })} />
        <div>
          <h5>{i18next.t("policy_delete_text")}</h5>
          <button
            class="uk-button uk-button-danger"
            onClick={async () => {
              try {
                await this.props.api.deletePolicy(policyName);
                route("/policies");
              } catch (e: unknown) {
                const error = e as Error;
                setErrorText(error.message);
              }
            }}
          >
            {i18next.t("policy_delete_btn")}
          </button>
        </div>
      </>
    );
  }
}
