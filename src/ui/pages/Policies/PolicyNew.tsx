import { Component } from "preact";
import { DefaultPageProps } from "../../../types/DefaultPageProps";
import { Form } from "../../elements/Form";
import { Margin } from "../../elements/Margin";
import { PageTitle } from "../../elements/PageTitle";
import { policyViewURL } from "../pageLinks";
import { route } from "preact-router";
import { setErrorText } from "../../../pageUtils";
import i18next from "i18next";

export class PolicyNew extends Component<DefaultPageProps> {
  render() {
    return (
      <>
        <PageTitle title={i18next.t("policy_new_title")} />
        <div>
          <Form
            onSubmit={async (formData) => {
              const name = formData.get("name") as string;
              if ((await this.props.api.getPolicies()).includes(name)) {
                setErrorText(i18next.t("policy_new_already_exists"));
                return;
              }

              try {
                await this.props.api.createOrUpdatePolicy(name, " ");
                route(policyViewURL(name));
              } catch (e: unknown) {
                const error = e as Error;
                setErrorText(error.message);
              }
            }}
          >
            <Margin>
              <input
                class="uk-input uk-form-width-medium"
                name="name"
                placeholder={i18next.t("policy_new_name_placeholder")}
                required
              />
            </Margin>
            <p class="uk-text-danger" id="errorText" />
            <button class="uk-button uk-button-primary" type="submit">
              {i18next.t("policy_new_create_btn")}
            </button>
          </Form>
        </div>
      </>
    );
  }
}
