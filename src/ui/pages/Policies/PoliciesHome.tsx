import { Component } from "preact";
import { DefaultPageProps } from "../../../types/DefaultPageProps";
import { Margin } from "../../elements/Margin";
import { PageTitle } from "../../elements/PageTitle";
import { policyNewURL, policyViewURL } from "../pageLinks";
import { prePageChecks } from "../../../pageUtils";
import { route } from "preact-router";
import i18next from "i18next";

export class PoliciesHome extends Component<DefaultPageProps, { policies: string[] }> {
  async componentDidMount() {
    if (!(await prePageChecks(this.props.api, this.props.settings))) return;

    let policies = await this.props.api.getPolicies();
    policies = policies.sort();
    policies = policies.filter(function (policy_name) {
      return policy_name !== "root";
    });
    this.setState({
      policies: policies,
    });
  }

  render() {
    if (!this.state.policies) return;

    return (
      <>
        <PageTitle title={i18next.t("policies_home_title")} />
        <div>
          <p>
            <button
              class="uk-button uk-button-primary"
              onClick={async () => {
                route(policyNewURL());
              }}
            >
              {i18next.t("policies_home_new_btn")}
            </button>
          </p>

          <Margin>
            <ul class="uk-nav uk-nav-default">
              {this.state.policies.map((policyName: string) => (
                <li>
                  <a
                    onClick={async () => {
                      route(policyViewURL(policyName));
                    }}
                  >
                    {policyName}
                  </a>
                </li>
              ))}
            </ul>
          </Margin>
        </div>
      </>
    );
  }
}
