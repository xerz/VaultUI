import { CodeEditor } from "../../elements/CodeEditor";
import { Component, JSX } from "preact";
import { DefaultPageProps } from "../../../types/DefaultPageProps";
import { Margin } from "../../elements/Margin";
import { MarginInline } from "../../elements/MarginInline";
import { PageTitle } from "../../elements/PageTitle";
import { policyViewURL } from "../pageLinks";
import { route } from "preact-router";
import { setErrorText } from "../../../pageUtils";
import i18next from "i18next";

type PolicyEditorProps = DefaultPageProps & {
  policyName: string;
};

type PolicyEditorState =
  | {
      dataLoaded: false;
    }
  | {
      dataLoaded: true;
      policyData: string;
      code: string;
    };

export class PolicyEditor extends Component<PolicyEditorProps, PolicyEditorState> {
  constructor() {
    super();
    this.state = {
      dataLoaded: false,
    };
  }

  async editorSave(): Promise<void> {
    if (!this.state.dataLoaded) return;

    try {
      await this.props.api.createOrUpdatePolicy(this.props.policyName, this.state.code);
      route(policyViewURL(this.props.policyName));
    } catch (e: unknown) {
      const error = e as Error;
      setErrorText(error.message);
    }
  }

  onCodeUpdate(code: string): void {
    this.setState({
      code: code,
    });
  }

  async loadData(): Promise<void> {
    const policyData = await this.props.api.getPolicy(this.props.policyName);
    this.setState({
      dataLoaded: true,
      policyData: policyData,
      code: policyData,
    });
    return;
  }

  componentDidMount(): void {
    if (!this.state.dataLoaded) {
      void this.loadData();
    }
  }

  render(): JSX.Element {
    if (!this.state.dataLoaded) {
      return <p>{i18next.t("content_loading")}</p>;
    }

    return (
      <div>
        <p class="uk-text-danger" id="errorText" />
        <Margin>
          <CodeEditor
            language="hcl"
            tabSize={2}
            code={this.state.policyData}
            onUpdate={(code) => this.onCodeUpdate(code)}
          />
        </Margin>
        <MarginInline>
          <button class="uk-button uk-button-primary" onClick={() => this.editorSave()}>
            {i18next.t("policy_edit_edit_btn")}
          </button>
        </MarginInline>
      </div>
    );
  }
}

export class PolicyEdit extends Component<DefaultPageProps> {
  render() {
    const policyName = this.props.matches["policyName"];
    return (
      <>
        <PageTitle title={i18next.t("policy_edit_title", { policy: policyName })} />
        <div>
          <PolicyEditor
            settings={this.props.settings}
            api={this.props.api}
            policyName={policyName}
          />
        </div>
      </>
    );
  }
}
