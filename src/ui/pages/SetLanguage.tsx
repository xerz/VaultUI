// @ts-ignore
import translations from "../../translations/index.mjs";
// ts-unignore

import { Component } from "preact";
import { DefaultPageProps } from "../../types/DefaultPageProps";
import { Form } from "../elements/Form";
import { Margin } from "../elements/Margin";
import { MarginInline } from "../elements/MarginInline";
import { PageTitle } from "../elements/PageTitle";
import { route } from "preact-router";
import i18next from "i18next";

const languageIDs = Object.getOwnPropertyNames(translations);

export class SetLanguage extends Component<DefaultPageProps> {
  constructor() {
    super();
  }
  render() {
    return (
      <>
        <PageTitle title={i18next.t("set_language_title")} />
        <Form onSubmit={(data) => this.onSubmit(data)}>
          <Margin>
            <select class="uk-select uk-form-width-large" name="language">
              {languageIDs.map((languageID) => (
                <option value={languageID}>
                  {i18next.getFixedT(languageID, null)("language_name")}
                </option>
              ))}
            </select>
          </Margin>
          <p class="uk-text-danger" id="errorText" />
          <MarginInline>
            <button class="uk-button uk-button-primary" type="submit">
              {i18next.t("set_language_btn")}
            </button>
          </MarginInline>
        </Form>
      </>
    );
  }

  async onSubmit(data: FormData): Promise<void> {
    const language = data.get("language") as string;
    this.props.settings.language = language;

    const t = await i18next.changeLanguage(language);
    this.props.settings.pageDirection = t("language_direction");
    // TODO: make navbar somethingy
    //reloadNavBar(this.router);
    route("/");
  }
}
