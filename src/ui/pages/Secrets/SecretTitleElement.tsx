import { JSX } from "preact/jsx-runtime";
import { kvListURL } from "../pageLinks";
import { route } from "preact-router";

type SecretTitleElementProps = {
  type: string;
  baseMount: string;
  secretPath?: string[];
  item?: string;
  suffix?: string;
};

export function SecretTitleElement(props: SecretTitleElementProps): JSX.Element {
  const type = props.type;
  const item = props.item || "";
  const suffix = props.suffix || "";
  const baseMount = props.baseMount;
  const secretPath = props.secretPath || [];

  return (
    <h3 class="uk-card-title" id="pageTitle">
      <div>
        <a
          test-data="secrets-title-first-slash"
          onClick={async () => {
            route("/secrets");
          }}
        >
          {"/ "}
        </a>

        <a
          href={"/secrets/" + type + "/list/" + baseMount + "/"}
          test-data="secrets-title-baseMount"
        >
          {baseMount + "/ "}
        </a>

        {...secretPath.map((secretPath, index, secretPaths) => {
          // TODO: find where a '' is returned so dont need this
          if (secretPath.length < 1) return;
          return (
            <a
              test-data="secrets-title-secretPath"
              onClick={async () => {
                if (type == "kv") {
                  const secretPath = secretPaths.slice(0, index + 1);
                  route(kvListURL(baseMount, secretPath));
                }
              }}
            >
              {secretPath + "/" + " "}
            </a>
          );
        })}
        {item.length != 0 && <span>{item}</span>}
        {suffix.length != 0 && <span>{suffix}</span>}
      </div>
    </h3>
  );
}
