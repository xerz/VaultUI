import { Component } from "preact";
import { DefaultPageProps } from "../../../../types/DefaultPageProps";
import { Form } from "../../../elements/Form";
import { Margin } from "../../../elements/Margin";
import { MarginInline } from "../../../elements/MarginInline";
import { PageTitle } from "../../../elements/PageTitle";
import { route } from "preact-router";
import { setErrorText } from "../../../../pageUtils";
import { totpListURL } from "../../pageLinks";
import i18next from "i18next";

export class NewTOTPEngine extends Component<DefaultPageProps> {
  render() {
    return (
      <>
        <PageTitle title={i18next.t("new_totp_engine_title")} />
        <Form onSubmit={(data) => this.submit(data)}>
          <Margin>
            <input
              class="uk-input uk-form-width-medium"
              name="name"
              type="text"
              placeholder={i18next.t("new_totp_engine_name_input")}
              required
            />
          </Margin>
          <p class="uk-text-danger" id="errorText" />
          <MarginInline>
            <button class="uk-button uk-button-primary" type="submit">
              {i18next.t("new_totp_engine_create_btn")}
            </button>
          </MarginInline>
        </Form>
        ,
      </>
    );
  }

  async submit(data: FormData): Promise<void> {
    const name = data.get("name") as string;

    try {
      await this.props.api.newMount({
        name: name,
        type: "totp",
      });
      route(totpListURL(name));
    } catch (e) {
      const error = e as Error;
      setErrorText(error.message);
    }
  }
}
