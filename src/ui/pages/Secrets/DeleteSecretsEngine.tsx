import { Component } from "preact";
import { DefaultPageProps } from "../../../types/DefaultPageProps";
import { Form } from "../../elements/Form";
import { MarginInline } from "../../elements/MarginInline";
import { PageTitle } from "../../elements/PageTitle";
import { route } from "preact-router";
import { setErrorText } from "../../../pageUtils";
import i18next from "i18next";

export class DeleteSecretsEngine extends Component<DefaultPageProps> {
  render() {
    return (
      <>
        <PageTitle
          title={i18next.t("delete_secrets_engine_title", { mount: this.props.matches["mount"] })}
        />
        <Form
          onSubmit={async () => {
            await this.onSubmit();
          }}
        >
          <p>{i18next.t("delete_secrets_engine_message")}</p>

          <p class="uk-text-danger" id="errorText" />

          <MarginInline>
            <button class="uk-button uk-button-danger" type="submit">
              {i18next.t("delete_secrets_engine_delete_btn")}
            </button>
          </MarginInline>
        </Form>
      </>
    );
  }

  async onSubmit(): Promise<void> {
    try {
      await this.props.api.deleteMount(this.props.matches["mount"]);
      route("/secrets");
    } catch (e: unknown) {
      const error = e as Error;
      setErrorText(error.message);
    }
  }
}
