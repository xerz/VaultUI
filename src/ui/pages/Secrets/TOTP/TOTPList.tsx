import { CapabilitiesType } from "../../../../api/types/capabilities";
import { Component, JSX } from "preact";
import { CopyableInputBox } from "../../../elements/CopyableInputBox";
import { DefaultPageProps } from "../../../../types/DefaultPageProps";
import { DoesNotExistError } from "../../../../types/internalErrors";
import { Grid, GridSizes } from "../../../elements/Grid";
import { MarginInline } from "../../../elements/MarginInline";
import { SecretTitleElement } from "../SecretTitleElement";
import { delSecretsEngineURL, totpNewURL } from "../../pageLinks";
import { removeDoubleSlash } from "../../../../utils";
import { route } from "preact-router";
import { setErrorText } from "../../../../pageUtils";
import i18next from "i18next";

type TOTPGridItemProps = DefaultPageProps & {
  baseMount: string;
  totpKey: string;
  canDelete: boolean;
};

export class RefreshingTOTPGridItem extends Component<TOTPGridItemProps, { totpValue: string }> {
  constructor() {
    super();
    this.state = { totpValue: "" };
  }
  timer: unknown;

  updateTOTPCode(): void {
    void this.props.api.getTOTPCode(this.props.baseMount, this.props.totpKey).then((code) => {
      this.setState({ totpValue: code });
    });
  }

  componentWillUnmount(): void {
    clearInterval(this.timer as number);
  }

  componentDidMount(): void {
    this.updateTOTPCode();
    this.timer = setInterval(() => {
      this.updateTOTPCode();
    }, 3000);
  }

  render(): JSX.Element {
    return (
      <Grid size={GridSizes.NORMAL}>
        <CopyableInputBox text={this.props.totpKey} copyable />
        <CopyableInputBox text={this.state.totpValue} copyable />
        <div>
          <MarginInline>
            {this.props.canDelete && (
              <button
                class="uk-button uk-button-danger"
                onClick={async () => {
                  route("/secrets/totp/delete/" + this.props.baseMount + "/" + this.props.totpKey);
                }}
              >
                {i18next.t("totp_view_secret_delete_btn")}
              </button>
            )}
          </MarginInline>
        </div>
      </Grid>
    );
  }
}

type TOTPListState = {
  capabilities?: CapabilitiesType;
  totpItems: Partial<TOTPGridItemProps>[];
};

export class TOTPList extends Component<DefaultPageProps, TOTPListState> {
  constructor() {
    super();
    this.refresher = undefined;
    this.state = { capabilities: null, totpItems: [] };
  }

  refresher: number;

  async componentDidMount() {
    const api = this.props.api;
    const baseMount = this.props.matches["baseMount"];
    const mountsPath = "/sys/mounts/" + baseMount;
    const caps = await api.getCapabilitiesPath([mountsPath, baseMount]);

    let totpItems: Partial<TOTPGridItemProps>[] = [];

    // TODO: tidy this up i guess
    try {
      totpItems = await Promise.all(
        Array.from(await api.getTOTPKeys(baseMount)).map(async (key) => {
          const totpCaps = await api.getCapsPath(removeDoubleSlash(baseMount + "/code/" + key));
          if (totpCaps.includes("read")) {
            return {
              baseMount: baseMount,
              totpKey: key,
              canDelete: totpCaps.includes("delete"),
            };
          }
        }),
      );
    } catch (e: unknown) {
      const error = e as Error;
      if (error != DoesNotExistError) {
        setErrorText(error.message);
      }
    }

    this.setState({
      capabilities: caps,
      totpItems: totpItems,
    });
  }

  render() {
    if (!this.state.capabilities) return;
    const baseMount = this.props.matches["baseMount"];

    const mountsPath = "/sys/mounts/" + baseMount;
    const mountCaps = this.state.capabilities[mountsPath];
    const totpCaps = this.state.capabilities[baseMount];

    return (
      <>
        <SecretTitleElement type="totp" baseMount={baseMount} />
        <div>
          <p>
            {totpCaps.includes("create") && (
              <button
                class="uk-button uk-button-primary"
                onClick={async () => {
                  route(totpNewURL(baseMount));
                }}
              >
                {i18next.t("totp_view_new_btn")}
              </button>
            )}
            {mountCaps.includes("delete") && (
              <button
                class="uk-button uk-button-danger"
                onClick={async () => {
                  route(delSecretsEngineURL(baseMount));
                }}
              >
                {i18next.t("totp_view_delete_btn")}
              </button>
            )}
          </p>
          <div id="totpList">
            {(() => {
              if (this.state.totpItems.length == 0) {
                return <p>{i18next.t("totp_view_empty")}</p>;
              } else {
                return this.state.totpItems.map((totpItem) => {
                  return (
                    <RefreshingTOTPGridItem
                      settings={this.props.settings}
                      api={this.props.api}
                      baseMount={totpItem.baseMount}
                      totpKey={totpItem.totpKey}
                      canDelete={totpItem.canDelete}
                    />
                  );
                });
              }
            })()}
          </div>
        </div>
      </>
    );
  }
}
