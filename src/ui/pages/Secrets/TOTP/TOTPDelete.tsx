import { Component } from "preact";
import { DefaultPageProps } from "../../../../types/DefaultPageProps";
import { SecretTitleElement } from "../SecretTitleElement";
import { route } from "preact-router";
import { totpListURL } from "../../pageLinks";
import i18next from "i18next";

export class TOTPDelete extends Component<DefaultPageProps> {
  render() {
    return (
      <>
        <SecretTitleElement
          type="totp"
          baseMount={this.props.matches["baseMount"]}
          item={this.props.matches["item"]}
          suffix={i18next.t("totp_delete_suffix")}
        />
        <div>
          <h5>{i18next.t("totp_delete_text")}</h5>
          <button
            class="uk-button uk-button-danger"
            onClick={async () => {
              const baseMount = this.props.matches["baseMount"];
              const item = this.props.matches["item"];
              await this.props.api.deleteTOTP(baseMount, item);
              route(totpListURL(baseMount));
            }}
          >
            {i18next.t("kv_delete_btn")}
          </button>
        </div>
      </>
    );
  }
}
