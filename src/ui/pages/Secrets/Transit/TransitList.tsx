import { CapabilitiesType } from "../../../../api/types/capabilities";
import { Component, JSX } from "preact";
import { DefaultPageProps } from "../../../../types/DefaultPageProps";
import { SecretTitleElement } from "../SecretTitleElement";
import { delSecretsEngineURL, transitNewSecretURL, transitViewSecretURL } from "../../pageLinks";
import { route } from "preact-router";
import i18next from "i18next";

type TransitViewListState = {
  contentLoaded: boolean;
  transitKeysList: string[];
};

export class TransitViewListItem extends Component<
  { baseMount: string } & DefaultPageProps,
  TransitViewListState
> {
  constructor() {
    super();
    this.state = {
      contentLoaded: false,
      transitKeysList: [],
    };
  }

  timer: unknown;

  getTransitKeys(): void {
    void this.props.api
      .getTransitKeys(this.props.baseMount)
      .then((keys) => {
        this.setState({
          contentLoaded: true,
          transitKeysList: keys,
        });
      })
      .catch((_) => {
        this.setState({
          contentLoaded: true,
          transitKeysList: [],
        });
      });
  }

  componentDidMount(): void {
    this.getTransitKeys();
  }

  render(): JSX.Element {
    if (!this.state.contentLoaded) {
      return <p>{i18next.t("content_loading")}</p>;
    }

    if (this.state.transitKeysList.length == 0) {
      return <p>{i18next.t("transit_view_none_here_text")}</p>;
    }

    return (
      <ul class="uk-nav uk-nav-default">
        {...this.state.transitKeysList.map((key) => (
          <li>
            <a
              onClick={async () => {
                route(transitViewSecretURL(this.props.baseMount, key));
              }}
            >
              {key}
            </a>
          </li>
        ))}
      </ul>
    );
  }
}

export class TransitList extends Component<DefaultPageProps, { caps: CapabilitiesType }> {
  async componentDidMount() {
    const baseMount = this.props.matches["baseMount"];
    const mountsPath = "/sys/mounts/" + baseMount;

    const caps = await this.props.api.getCapabilitiesPath([mountsPath, baseMount]);
    this.setState({ caps });
  }

  render() {
    if (!this.state.caps) return;
    const baseMount = this.props.matches["baseMount"];
    const mountsPath = "/sys/mounts/" + baseMount;
    const mountCaps = this.state.caps[mountsPath];
    const transitCaps = this.state.caps[baseMount];

    return (
      <>
        <SecretTitleElement type="transit" baseMount={baseMount} />

        <p>
          {transitCaps.includes("create") && (
            <button
              class="uk-button uk-button-primary"
              onClick={async () => {
                route(transitNewSecretURL(baseMount));
              }}
            >
              {i18next.t("transit_view_new_btn")}
            </button>
          )}
          {mountCaps.includes("delete") && (
            <button
              class="uk-button uk-button-danger"
              onClick={async () => {
                route(delSecretsEngineURL(baseMount));
              }}
            >
              {i18next.t("transit_view_delete_btn")}
            </button>
          )}
        </p>
        <TransitViewListItem
          settings={this.props.settings}
          api={this.props.api}
          baseMount={baseMount}
        />
      </>
    );
  }
}
