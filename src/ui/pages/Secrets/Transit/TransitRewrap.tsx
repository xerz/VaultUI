import { Component, render } from "preact";
import { CopyableModal } from "../../../elements/CopyableModal";
import { DefaultPageProps } from "../../../../types/DefaultPageProps";
import { Form } from "../../../elements/Form";
import { Margin } from "../../../elements/Margin";
import { SecretTitleElement } from "../SecretTitleElement";
import { TransitKeyType } from "../../../../api/types/transit";
import { objectToMap } from "../../../../utils";
import { setErrorText } from "../../../../pageUtils";
import UIkit from "uikit";
import i18next from "i18next";

type versionOption = { version: string; label: string };

export class TransitRewrap extends Component<DefaultPageProps, { transitKey: TransitKeyType }> {
  async componentDidMount() {
    const baseMount = this.props.matches["baseMount"];
    const secretItem = this.props.matches["secretItem"];
    this.setState({
      transitKey: await this.props.api.getTransitKey(baseMount, secretItem),
    });
  }

  render() {
    if (!this.state.transitKey) return;

    const baseMount = this.props.matches["baseMount"];
    const secretItem = this.props.matches["secretItem"];

    const stringVersions = Array.from(
      objectToMap(this.state.transitKey.keys).keys(),
    ).reverse() as unknown as string[];

    const versions = stringVersions.map((val): number => parseInt(val, 10));

    // get the selectable version options in the same
    // format the official UI uses.
    // e.g: ["2 (latest)", "1"]

    const options: versionOption[] = versions.map((val): versionOption => {
      const i18nkey =
        val == Math.max(...versions)
          ? "transit_rewrap_latest_version_option_text"
          : "transit_rewrap_version_option_text";
      return {
        version: String(val),
        label: i18next.t(i18nkey, { version_num: String(val) }),
      };
    });

    return (
      <>
        <SecretTitleElement
          type="transit"
          baseMount={baseMount}
          item={secretItem}
          suffix={i18next.t("transit_rewrap_suffix")}
        />
        <Form onSubmit={async (data) => await this.onSubmit(data)}>
          <Margin>
            <select class="uk-select uk-width-1-2" name="version">
              {options.map((option) => (
                <option label={option.label} value={option.version}>
                  {option.label}
                </option>
              ))}
            </select>
          </Margin>
          <Margin>
            <textarea
              class="uk-textarea uk-width-1-2"
              name="ciphertext"
              placeholder={i18next.t("transit_rewrap_input_placeholder")}
            />
          </Margin>
          <p class="uk-text-danger" id="errorText" />
          <button class="uk-button uk-button-primary" type="submit">
            {i18next.t("transit_rewrap_rewrap_btn")}
          </button>
          <div id="modalAttachmentPoint" />
        </Form>
      </>
    );
  }

  async onSubmit(data: FormData): Promise<void> {
    const baseMount = this.props.matches["baseMount"];
    const secretItem = this.props.matches["secretItem"];
    try {
      const res = await this.props.api.transitRewrap(baseMount, secretItem, {
        ciphertext: data.get("ciphertext") as string,
        key_version: parseInt(data.get("version") as string, 10),
      });
      render(
        <CopyableModal
          id="transitResultModal"
          name={i18next.t("transit_rewrap_result_modal_title")}
          contentString={res.ciphertext}
        />,
        document.querySelector("#modalAttachmentPoint"),
      );
      UIkit.modal(document.querySelector("#transitResultModal")).show();
    } catch (e: unknown) {
      const error = e as Error;
      setErrorText(`API Error: ${error.message}`);
    }
  }
}
