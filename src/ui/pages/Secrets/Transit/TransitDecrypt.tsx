import { Component, render } from "preact";
import { CopyableModal } from "../../../elements/CopyableModal";
import { DefaultPageProps } from "../../../../types/DefaultPageProps";
import { FileUploadInput } from "../../../elements/FileUploadInput";
import { Form } from "../../../elements/Form";
import { InputWithTitle } from "../../../elements/InputWithTitle";
import { Margin } from "../../../elements/Margin";
import { SecretTitleElement } from "../SecretTitleElement";
import { fileToBase64 } from "../../../../htmlUtils";
import { setErrorText } from "../../../../pageUtils";
import UIkit from "uikit";
import i18next from "i18next";

export class TransitDecrypt extends Component<DefaultPageProps> {
  render() {
    const baseMount = this.props.matches["baseMount"];
    const secretItem = this.props.matches["secretItem"];
    return (
      <>
        <SecretTitleElement
          type="transit"
          baseMount={baseMount}
          item={secretItem}
          suffix={i18next.t("transit_decrypt_suffix")}
        />
        <Form onSubmit={async (data) => await this.onSubmit(data)}>
          <Margin>
            <textarea
              class="uk-textarea uk-form-width-medium"
              name="ciphertext"
              placeholder={i18next.t("transit_decrypt_input_placeholder")}
            />
          </Margin>
          <Margin>
            <FileUploadInput name="ciphertext_file" />
          </Margin>
          <InputWithTitle title={i18next.t("transit_decrypt_decode_checkbox")}>
            <input class="uk-checkbox" name="decodeBase64Checkbox" type="checkbox" />
          </InputWithTitle>
          <p class="uk-text-danger" id="errorText" />
          <button class="uk-button uk-button-primary" type="submit">
            {i18next.t("transit_decrypt_decrypt_btn")}
          </button>
          <div id="modalAttachmentPoint" />
        </Form>
      </>
    );
  }

  async onSubmit(data: FormData): Promise<void> {
    const baseMount = this.props.matches["baseMount"];
    const secretItem = this.props.matches["secretItem"];

    const decodeBase64 = data.get("decodeBase64Checkbox") as string;

    let ciphertext = data.get("ciphertext") as string;

    const ciphertext_file = data.get("ciphertext_file") as File;
    if (ciphertext_file.size > 0) {
      ciphertext = atob(
        (await fileToBase64(ciphertext_file)).replace("data:text/plain;base64,", ""),
      );
    }

    try {
      const res = await this.props.api.transitDecrypt(baseMount, secretItem, {
        ciphertext: ciphertext,
      });
      let plaintext = res.plaintext;
      if (decodeBase64 == "on") {
        plaintext = atob(plaintext);
      }
      render(
        <CopyableModal
          id="transitResultModal"
          name={i18next.t("transit_decrypt_decryption_result_modal_title")}
          contentString={plaintext}
        />,
        document.querySelector("#modalAttachmentPoint"),
      );
      UIkit.modal(document.querySelector("#transitResultModal")).show();
    } catch (e: unknown) {
      const error = e as Error;
      setErrorText(`API Error: ${error.message}`);
    }
  }
}
