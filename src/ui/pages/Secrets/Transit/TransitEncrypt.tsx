import { Component, render } from "preact";
import { CopyableModal } from "../../../elements/CopyableModal";
import { DefaultPageProps } from "../../../../types/DefaultPageProps";
import { FileUploadInput } from "../../../elements/FileUploadInput";
import { Form } from "../../../elements/Form";
import { InputWithTitle } from "../../../elements/InputWithTitle";
import { Margin } from "../../../elements/Margin";
import { SecretTitleElement } from "../SecretTitleElement";
import { fileToBase64 } from "../../../../htmlUtils";
import { setErrorText } from "../../../../pageUtils";
import UIkit from "uikit";
import i18next from "i18next";

export class TransitEncrypt extends Component<DefaultPageProps> {
  render() {
    const baseMount = this.props.matches["baseMount"];
    const secretItem = this.props.matches["secretItem"];
    return (
      <>
        <SecretTitleElement
          type="transit"
          baseMount={baseMount}
          item={secretItem}
          suffix={i18next.t("transit_encrypt_suffix")}
        />
        <Form onSubmit={async (data) => await this.onSubmit(data)}>
          <Margin>
            <textarea
              class="uk-textarea uk-form-width-medium"
              name="plaintext"
              placeholder={i18next.t("transit_encrypt_input_placeholder")}
            />
          </Margin>
          <Margin>
            <FileUploadInput name="plaintext_file" />
          </Margin>
          <InputWithTitle title={i18next.t("transit_encrypt_already_encoded_checkbox")}>
            <input class="uk-checkbox" name="base64Checkbox" type="checkbox" />
          </InputWithTitle>
          <p class="uk-text-danger" id="errorText" />
          <button class="uk-button uk-button-primary" type="submit">
            {i18next.t("transit_encrypt_encrypt_btn")}
          </button>
          <div id="modalAttachmentPoint" />
        </Form>
      </>
    );
  }

  async onSubmit(data: FormData): Promise<void> {
    const baseMount = this.props.matches["baseMount"];
    const secretItem = this.props.matches["secretItem"];

    const base64Checkbox = data.get("base64Checkbox") as string;

    let plaintext = data.get("plaintext") as string;

    const plaintext_file = data.get("plaintext_file") as File;
    if (plaintext_file.size > 0) {
      plaintext = (await fileToBase64(plaintext_file)).replace("data:text/plain;base64,", "");
      plaintext = base64Checkbox == "on" ? atob(plaintext) : plaintext;
    } else {
      plaintext = base64Checkbox == "on" ? plaintext : btoa(plaintext);
    }

    try {
      const res = await this.props.api.transitEncrypt(baseMount, secretItem, {
        plaintext: plaintext,
      });
      render(
        <CopyableModal
          id="transitResultModal"
          name={i18next.t("transit_encrypt_encryption_result_modal_title")}
          contentString={res.ciphertext}
        />,
        document.querySelector("#modalAttachmentPoint"),
      );
      UIkit.modal(document.querySelector("#transitResultModal")).show();
    } catch (e: unknown) {
      const error = e as Error;
      setErrorText(`API Error: ${error.message}`);
    }
  }
}
