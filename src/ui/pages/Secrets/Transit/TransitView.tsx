import { Component } from "preact";
import { DefaultPageProps } from "../../../../types/DefaultPageProps";
import { Grid, GridSizes } from "../../../elements/Grid";
import { SecretTitleElement } from "../SecretTitleElement";
import { Tile } from "../../../elements/Tile";
import { TransitKeyType } from "../../../../api/types/transit";
import { route } from "preact-router";
import {
  transitDecryptSecretURL,
  transitEncryptSecretURL,
  transitRewrapSecretURL,
} from "../../pageLinks";
import i18next from "i18next";

export class TransitView extends Component<DefaultPageProps, { transitKey: TransitKeyType }> {
  async componentDidMount() {
    const baseMount = this.props.matches["baseMount"];
    const secretItem = this.props.matches["secretItem"];
    const transitKey = await this.props.api.getTransitKey(baseMount, secretItem);
    this.setState({ transitKey });
  }

  render() {
    if (!this.state.transitKey) return;
    const baseMount = this.props.matches["baseMount"];
    const secretItem = this.props.matches["secretItem"];
    const transitKey = this.state.transitKey;

    return (
      <>
        <SecretTitleElement type="transit" baseMount={baseMount} item={secretItem} />
        <Grid size={GridSizes.MATCHING_TWO_ROWS}>
          {transitKey.supports_encryption && (
            <Tile
              title={i18next.t("transit_view_encrypt_text")}
              description={i18next.t("transit_view_encrypt_description")}
              icon="lock"
              iconText={i18next.t("transit_view_encrypt_icon_text")}
              onclick={async () => route(transitEncryptSecretURL(baseMount, secretItem))}
            />
          )}
          {transitKey.supports_decryption && (
            <Tile
              title={i18next.t("transit_view_decrypt_text")}
              description={i18next.t("transit_view_decrypt_description")}
              icon="mail"
              iconText={i18next.t("transit_view_decrypt_icon_text")}
              onclick={() => route(transitDecryptSecretURL(baseMount, secretItem))}
            />
          )}
          {transitKey.supports_decryption && (
            <Tile
              title={i18next.t("transit_view_rewrap_text")}
              description={i18next.t("transit_view_rewrap_description")}
              icon="code"
              iconText={i18next.t("transit_view_rewrap_icon_text")}
              onclick={async () => route(transitRewrapSecretURL(baseMount, secretItem))}
            />
          )}
        </Grid>
      </>
    );
  }
}
