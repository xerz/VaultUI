import { Component } from "preact";
import { DefaultPageProps } from "../../../../types/DefaultPageProps";
import { SecretTitleElement } from "../SecretTitleElement";
import i18next from "i18next";

export class KeyValueDelete extends Component<DefaultPageProps> {
  render() {
    const baseMount = this.props.matches["baseMount"];
    const secretPath = this.props.matches["secretPath"].split("/");
    const item = this.props.matches["item"];

    return (
      <>
        <SecretTitleElement
          type="kv"
          baseMount={baseMount}
          secretPath={secretPath}
          item={item}
          suffix={i18next.t("kv_sec_edit_suffix")}
        />
        <div>
          <h5>{i18next.t("kv_delete_text")}</h5>
          <button
            class="uk-button uk-button-danger"
            onClick={async () => {
              await this.props.api.deleteSecret(baseMount, secretPath, item);
              window.history.back();
            }}
          >
            {i18next.t("kv_delete_btn")}
          </button>
        </div>
      </>
    );
  }
}
