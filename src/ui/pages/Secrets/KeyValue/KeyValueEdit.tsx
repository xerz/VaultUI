import { CodeEditor } from "../../../elements/CodeEditor";
import { Component, JSX } from "preact";
import { DefaultPageProps } from "../../../../types/DefaultPageProps";
import { SecretTitleElement } from "../SecretTitleElement";
import { setErrorText } from "../../../../pageUtils";
import { sortedObjectMap, verifyJSONString } from "../../../../utils";
import i18next from "i18next";

export type KVEditProps = DefaultPageProps & {
  baseMount: string;
  secretPath: string[];
  secretItem: string;
};

type KVEditState =
  | {
      dataLoaded: false;
    }
  | {
      dataLoaded: true;
      kvData: Record<string, unknown>;
      code: string;
    };

export class KVEditor extends Component<KVEditProps, KVEditState> {
  constructor() {
    super();
    this.state = {
      dataLoaded: false,
    };
  }

  async editorSave(): Promise<void> {
    if (!this.state.dataLoaded) return;
    const editorContent = this.state.code;

    if (!verifyJSONString(editorContent)) {
      setErrorText(i18next.t("kv_sec_edit_invalid_json_err"));
      return;
    }

    await this.props.api.createOrUpdateSecret(
      this.props.baseMount,
      this.props.secretPath.map((e) => e + "/"),
      this.props.secretItem,
      JSON.parse(editorContent) as unknown as Record<string, unknown>,
    );
    window.history.back();
  }

  onCodeUpdate(code: string): void {
    this.setState({
      code: code,
    });
  }

  loadData(): void {
    void this.props.api
      .getSecret(
        this.props.baseMount,
        this.props.secretPath.map((e) => e + "/"),
        this.props.secretItem,
      )
      .then((kvData) => {
        this.setState({
          dataLoaded: true,
          kvData: kvData,
          code: this.getStringKVData(kvData),
        });
      });
    return;
  }

  componentDidMount(): void {
    if (!this.state.dataLoaded) {
      this.loadData();
    }
  }

  getStringKVData(data: Record<string, unknown>): string {
    return JSON.stringify(Object.fromEntries(sortedObjectMap(data)), null, 4);
  }

  render(): JSX.Element {
    if (!this.state.dataLoaded) {
      return <p>{i18next.t("content_loading")}</p>;
    }

    return (
      <div>
        <p class="uk-text-danger" id="errorText" />
        <CodeEditor
          language="json"
          tabSize={4}
          code={this.getStringKVData(this.state.kvData)}
          onUpdate={(code) => this.onCodeUpdate(code)}
        />
        <button class="uk-button uk-button-primary" onClick={() => this.editorSave()}>
          {i18next.t("kv_sec_edit_btn")}
        </button>
      </div>
    );
  }
}

export class KeyValueEdit extends Component<DefaultPageProps> {
  render() {
    const baseMount = this.props.matches["baseMount"];
    const secretPath = this.props.matches["secretPath"].split("/");
    const item = this.props.matches["item"];

    return (
      <>
        <SecretTitleElement
          type="kv"
          baseMount={baseMount}
          secretPath={secretPath}
          item={this.props.matches["item"]}
          suffix={i18next.t("kv_sec_edit_suffix")}
        />
        <KVEditor
          settings={this.props.settings}
          api={this.props.api}
          baseMount={baseMount}
          secretPath={secretPath}
          secretItem={item}
        />
      </>
    );
  }
}
