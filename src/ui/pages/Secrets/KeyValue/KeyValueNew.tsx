import { Component } from "preact";
import { DefaultPageProps } from "../../../../types/DefaultPageProps";
import { Form } from "../../../elements/Form";
import { Margin } from "../../../elements/Margin";
import { SecretTitleElement } from "../SecretTitleElement";
import { kvViewURL } from "../../pageLinks";
import { route } from "preact-router";
import { setErrorText } from "../../../../pageUtils";
import i18next from "i18next";

export class KeyValueNew extends Component<DefaultPageProps> {
  render() {
    const baseMount = this.props.matches["baseMount"];
    const secretPath = (this.props.matches["secretPath"] || "").split("/");

    return (
      <>
        <SecretTitleElement
          type="kv"
          baseMount={baseMount}
          secretPath={secretPath}
          suffix={i18next.t("kv_sec_edit_suffix")}
        />
        <div>
          <Form
            onSubmit={async (formData) =>
              await this.newKVSecretHandleForm(formData, baseMount, secretPath)
            }
          >
            <Margin>
              <input
                class="uk-input uk-form-width-medium"
                name="path"
                placeholder={i18next.t("kv_new_path")}
                required
              />
            </Margin>
            <p class="uk-text-danger" id="errorText" />
            <button class="uk-button uk-button-primary" type="submit">
              {i18next.t("kv_new_create_btn")}
            </button>
          </Form>
        </div>
      </>
    );
  }

  async newKVSecretHandleForm(
    formData: FormData,
    baseMount: string,
    secretPath: string[],
  ): Promise<void> {
    const path = formData.get("path") as string;

    // TODO: check only do this on kv v1
    const keyData = { key: "value" };

    try {
      await this.props.api.createOrUpdateSecret(baseMount, secretPath, path, keyData);
      route(kvViewURL(baseMount, secretPath, path));
    } catch (e: unknown) {
      const error = e as Error;
      setErrorText(error.message);
    }
  }
}
