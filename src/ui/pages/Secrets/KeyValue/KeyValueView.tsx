import { CodeBlock } from "../../../elements/CodeBlock";
import { Component, JSX } from "preact";
import { CopyableInputBox } from "../../../elements/CopyableInputBox";
import { DefaultPageProps } from "../../../../types/DefaultPageProps";
import { Grid, GridSizes } from "../../../elements/Grid";
import { SecretTitleElement } from "../SecretTitleElement";
import { kvDeleteURL, kvEditURL } from "../../pageLinks";
import { route } from "preact-router";
import { sortedObjectMap } from "../../../../utils";
import i18next from "i18next";

export type KVSecretViewProps = {
  kvData: Record<string, unknown>;
};

export class KVSecretVew extends Component<KVSecretViewProps, unknown> {
  render(): JSX.Element {
    const secretsMap = sortedObjectMap(this.props.kvData);
    let isMultiLevelJSON = false;

    for (const value of secretsMap.values()) {
      if (typeof value == "object") isMultiLevelJSON = true;
    }

    if (isMultiLevelJSON) {
      const jsonText = JSON.stringify(Object.fromEntries(secretsMap), null, 4);
      return <CodeBlock language="json" code={jsonText} />;
    } else {
      return (
        <>
          {Array.from(secretsMap).map((data: [string, string]) => (
            <Grid size={GridSizes.NORMAL}>
              <CopyableInputBox text={data[0]} copyable />
              <CopyableInputBox text={data[1]} copyable />
            </Grid>
          ))}
        </>
      );
    }
  }
}

type KeyValueViewState = {
  baseMount: string;
  secretPath: string[];
  secretItem: string;
  caps: string[];
  secretInfo: Record<string, unknown>;
};

export class KeyValueView extends Component<DefaultPageProps, KeyValueViewState> {
  async componentDidMount() {
    const baseMount = this.props.matches["baseMount"];
    const secretPath = this.props.matches["secretPath"].split("/");
    const secretItem = this.props.matches["item"];

    const caps = (await this.props.api.getCapabilities(baseMount, secretPath, secretItem))
      .capabilities;

    const secretPathAPI = secretPath.map((e) => e + "/");
    // TODO: this is a big hacky, fix when redo how api arguments work
    secretPathAPI[secretPathAPI.length - 1] = String(secretPathAPI[secretPathAPI.length - 1])
      .replace("/", "")
      .toString();

    const secretInfo = await this.props.api.getSecret(baseMount, secretPathAPI, secretItem);
    this.setState({
      baseMount,
      secretPath,
      secretItem,
      caps,
      secretInfo,
    });
  }
  render() {
    if (!this.state.baseMount) return;

    return (
      <>
        <SecretTitleElement
          type="kv"
          item={this.props.matches["item"]}
          baseMount={this.state.baseMount}
          secretPath={this.state.secretPath}
          suffix={i18next.t("kv_sec_edit_suffix")}
        />
        <div>
          <p id="buttonsBlock">
            {this.state.caps.includes("delete") && (
              <button
                class="uk-button uk-button-danger"
                onClick={async () => {
                  route(
                    kvDeleteURL(this.state.baseMount, this.state.secretPath, this.state.secretItem),
                  );
                }}
              >
                {i18next.t("kv_secret_delete_btn")}
              </button>
            )}
            {this.state.caps.includes("update") && (
              <button
                class="uk-button uk-button-primary"
                onClick={async () => {
                  route(
                    kvEditURL(this.state.baseMount, this.state.secretPath, this.state.secretItem),
                  );
                }}
              >
                {i18next.t("kv_secret_edit_btn")}
              </button>
            )}
          </p>

          {<KVSecretVew kvData={this.state.secretInfo} />}
        </div>
      </>
    );
  }
}
