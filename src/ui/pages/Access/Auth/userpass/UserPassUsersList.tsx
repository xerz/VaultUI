import { Component } from "preact";
import { DefaultPageProps } from "../../../../../types/DefaultPageProps";
import { PageTitle } from "../../../../elements/PageTitle";
import { route } from "preact-router";
import { userPassUserNewURL, userPassUserViewURL } from "../../../pageLinks";
import i18next from "i18next";

export class UserPassUsersList extends Component<DefaultPageProps, { users: string[] }> {
  async componentDidMount() {
    const baseMount = this.props.matches["baseMount"];
    const users = await this.props.api.listUserPassUsers(baseMount);
    this.setState({ users });
  }

  render() {
    if (!this.state.users) return;
    const baseMount = this.props.matches["baseMount"];

    return (
      <>
        <PageTitle title={i18next.t("userpass_users_list_title")} />
        <div>
          <button
            class="uk-button uk-margin uk-button-primary"
            type="submit"
            onClick={async () => {
              route(userPassUserNewURL(baseMount));
            }}
          >
            {i18next.t("userpass_user_list_new_btn")}
          </button>

          <ul>
            {...this.state.users.map((user) => (
              <li>
                <a
                  onClick={async () => {
                    route(userPassUserViewURL(baseMount, user));
                  }}
                >
                  {user}
                </a>
              </li>
            ))}
          </ul>
        </div>
      </>
    );
  }
}
