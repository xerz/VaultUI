import { Component } from "preact";
import { DefaultPageProps } from "../../../../../types/DefaultPageProps";
import { Form } from "../../../../elements/Form";
import { Margin } from "../../../../elements/Margin";
import { MarginInline } from "../../../../elements/MarginInline";
import { PageTitle } from "../../../../elements/PageTitle";
import { UserType } from "../../../../../api/types/user";
import { route } from "preact-router";
import { setErrorText } from "../../../../../pageUtils";
import { userPassUserViewURL } from "../../../pageLinks";
import i18next from "i18next";

export class UserPassUserNew extends Component<DefaultPageProps> {
  render() {
    return (
      <>
        <PageTitle title={i18next.t("userpass_user_new_title")} />
        <Form onSubmit={(data) => this.onSubmit(data)}>
          <Margin>
            <input
              class="uk-input uk-form-width-large"
              name="username"
              type="text"
              placeholder={i18next.t("auth_common_username")}
            />
          </Margin>
          <Margin>
            <input
              class="uk-input uk-form-width-large"
              name="password"
              type="password"
              placeholder={i18next.t("auth_common_password")}
            />
          </Margin>
          <p class="uk-text-danger" id="errorText" />
          <MarginInline>
            <button class="uk-button uk-button-primary" type="submit">
              {i18next.t("userpass_user_new_create_btn")}
            </button>
          </MarginInline>
        </Form>
      </>
    );
  }

  async onSubmit(data: FormData): Promise<void> {
    const baseMount = this.props.matches["baseMount"];
    const apiData: Partial<UserType> = {
      password: data.get("password") as string,
    };
    try {
      await this.props.api.createOrUpdateUserPassUser(
        baseMount,
        data.get("username") as string,
        apiData,
      );
      route(userPassUserViewURL(baseMount, data.get("username") as string));
    } catch (e: unknown) {
      const error = e as Error;
      setErrorText(error.message);
    }
  }
}
