import { Component } from "preact";
import { DefaultPageProps } from "../../../../../types/DefaultPageProps";
import { PageTitle } from "../../../../elements/PageTitle";
import { route } from "preact-router";
import { userPassUserListURL } from "../../../pageLinks";
import i18next from "i18next";

export class UserPassUserDelete extends Component<DefaultPageProps> {
  render() {
    const baseMount = this.props.matches["baseMount"];
    const user = this.props.matches["user"];

    return (
      <>
        <PageTitle title={i18next.t("userpass_user_delete_title")} />
        <div>
          <h5>{i18next.t("userpass_user_delete_text")}</h5>
          <button
            class="uk-button uk-button-danger"
            onClick={async () => {
              await this.props.api.deleteUserPassUser(baseMount, user);
              route(userPassUserListURL(baseMount));
            }}
          >
            {i18next.t("userpass_user_delete_btn")}
          </button>
        </div>
      </>
    );
  }
}
