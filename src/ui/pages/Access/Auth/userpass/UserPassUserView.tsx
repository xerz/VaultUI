import { Component } from "preact";
import { DefaultPageProps } from "../../../../../types/DefaultPageProps";
import { HeaderAndContent } from "../../../../elements/HeaderAndContent";
import { Margin } from "../../../../elements/Margin";
import { PageTitle } from "../../../../elements/PageTitle";
import { UserType } from "../../../../../api/types/user";
import { route } from "preact-router";
import { toStr } from "../../../../../utils";
import { userPassUserDeleteURL, userPassUserEditURL } from "../../../pageLinks";
import i18next from "i18next";

export class UserPassUserView extends Component<DefaultPageProps, { user_data: UserType }> {
  async componentDidMount() {
    const baseMount = this.props.matches["baseMount"];
    const user = this.props.matches["user"];

    const user_data = await this.props.api.getUserPassUser(baseMount, user);
    this.setState({ user_data });
  }

  render() {
    if (!this.state.user_data) return;
    const baseMount = this.props.matches["baseMount"];
    const user = this.props.matches["user"];
    const user_data = this.state.user_data;

    return (
      <>
        <PageTitle title={i18next.t("userpass_user_view_title")} />

        <div>
          <p>
            <button
              class="uk-button uk-button-danger"
              onClick={async () => {
                route(userPassUserDeleteURL(baseMount, user));
              }}
            >
              {i18next.t("userpass_user_view_delete_btn")}
            </button>
            <button
              class="uk-button uk-button-primary"
              onClick={async () => {
                route(userPassUserEditURL(baseMount, user));
              }}
            >
              {i18next.t("userpass_user_view_edit_btn")}
            </button>
          </p>

          <Margin>
            <p>{i18next.t("auth_common_zero_default")}</p>
          </Margin>

          <Margin>
            <p>{i18next.t("auth_common_generated_tokens")}</p>
          </Margin>

          <table class="uk-table">
            <tbody>
              <HeaderAndContent
                title={i18next.t("auth_common_cidrs")}
                content={user_data.token_bound_cidrs.join()}
              />
              <HeaderAndContent
                title={i18next.t("auth_common_exp_max_ttl")}
                content={toStr(user_data.token_explicit_max_ttl)}
              />
              <HeaderAndContent
                title={i18next.t("auth_common_max_ttl")}
                content={toStr(user_data.token_max_ttl)}
              />
              <HeaderAndContent
                title={i18next.t("auth_common_default_policy_attached")}
                content={toStr(user_data.token_no_default_policy)}
              />
              <HeaderAndContent
                title={i18next.t("auth_common_max_token_uses")}
                content={toStr(user_data.token_num_uses)}
              />
              <HeaderAndContent
                title={i18next.t("auth_common_token_peroid")}
                content={toStr(user_data.token_period)}
              />
              <HeaderAndContent
                title={i18next.t("auth_common_policies")}
                content={user_data.token_policies.join()}
              />
              <HeaderAndContent
                title={i18next.t("auth_common_initial_ttl")}
                content={toStr(user_data.token_ttl)}
              />
              <HeaderAndContent
                title={i18next.t("auth_common_type")}
                content={toStr(user_data.token_type)}
              />
            </tbody>
          </table>
        </div>
      </>
    );
  }
}
