import { Component } from "preact";
import { DefaultPageProps } from "../../../../../types/DefaultPageProps";
import { Form } from "../../../../elements/Form";
import { InputWithTitle } from "../../../../elements/InputWithTitle";
import { Margin } from "../../../../elements/Margin";
import { MarginInline } from "../../../../elements/MarginInline";
import { PageTitle } from "../../../../elements/PageTitle";
import { UserType } from "../../../../../api/types/user";
import { route } from "preact-router";
import { setErrorText } from "../../../../../pageUtils";
import { toStr } from "../../../../../utils";
import { userPassUserViewURL } from "../../../pageLinks";
import i18next from "i18next";

const removeEmptyStrings = (arr: string[]) => arr.filter((e) => e.length > 0);

export class UserPassUserEdit extends Component<DefaultPageProps, { user_data: UserType }> {
  async componentDidMount() {
    const baseMount = this.props.matches["baseMount"];
    const user = this.props.matches["user"];

    const user_data = await this.props.api.getUserPassUser(baseMount, user);
    this.setState({ user_data });
  }

  render() {
    if (!this.state.user_data) return;
    const user_data = this.state.user_data;

    return (
      <>
        <PageTitle title={i18next.t("userpass_user_edit_title")} />
        <Form onSubmit={(data) => this.onSubmit(data)}>
          <input
            class="uk-input uk-form-width-large"
            name="password"
            type="password"
            placeholder={i18next.t("auth_common_password")}
          />

          <Margin>
            <p>{i18next.t("auth_common_zero_default")}</p>
          </Margin>

          <Margin>
            <p>{i18next.t("auth_common_generated_tokens")}</p>
          </Margin>

          <InputWithTitle title={i18next.t("auth_common_cidrs")}>
            <input
              class="uk-input uk-form-width-large"
              name="cidrs"
              type="text"
              value={user_data.token_bound_cidrs.join()}
            />
          </InputWithTitle>
          <InputWithTitle title={i18next.t("auth_common_exp_max_ttl")}>
            <input
              class="uk-input uk-form-width-large"
              name="exp_max_ttl"
              type="number"
              value={toStr(user_data.token_explicit_max_ttl)}
            />
          </InputWithTitle>
          <InputWithTitle title={i18next.t("auth_common_max_ttl")}>
            <input
              class="uk-input uk-form-width-large"
              name="max_ttl"
              type="number"
              value={toStr(user_data.token_max_ttl)}
            />
          </InputWithTitle>
          <InputWithTitle title={i18next.t("auth_common_default_policy_attached")}>
            <input
              class="uk-checkbox"
              name="def_pol_attached"
              type="checkbox"
              value={toStr(user_data.token_no_default_policy)}
            />
          </InputWithTitle>
          <InputWithTitle title={i18next.t("auth_common_max_token_uses")}>
            <input
              class="uk-input uk-form-width-large"
              name="max_uses"
              type="number"
              value={toStr(user_data.token_num_uses)}
            />
          </InputWithTitle>
          <InputWithTitle title={i18next.t("auth_common_token_peroid")}>
            <input
              class="uk-input uk-form-width-large"
              name="period"
              type="number"
              value={toStr(user_data.token_period)}
            />
          </InputWithTitle>
          <InputWithTitle title={i18next.t("auth_common_policies")}>
            <input
              class="uk-input uk-form-width-large"
              name="policies"
              type="text"
              value={user_data.token_policies.join()}
            />
          </InputWithTitle>
          <InputWithTitle title={i18next.t("auth_common_initial_ttl")}>
            <input
              class="uk-input uk-form-width-large"
              name="initial_ttl"
              type="number"
              value={toStr(user_data.token_ttl)}
            />
          </InputWithTitle>
          <p class="uk-text-danger" id="errorText" />
          <MarginInline>
            <button class="uk-button uk-button-primary" type="submit">
              {i18next.t("userpass_user_edit_submit_btn")}
            </button>
          </MarginInline>
        </Form>
      </>
    );
  }

  async onSubmit(data: FormData): Promise<void> {
    const baseMount = this.props.matches["baseMount"];
    const user = this.props.matches["user"];

    const apiData: Partial<UserType> = {
      token_bound_cidrs: removeEmptyStrings(String(data.get("cidrs")).split(",")),
      token_explicit_max_ttl: parseInt(data.get("exp_max_ttl") as string, 10),
      token_max_ttl: parseInt(data.get("max_ttl") as string, 10),
      token_no_default_policy: (data.get("def_pol_attached") as string) == "true",
      token_num_uses: parseInt(data.get("max_uses") as string, 10),
      token_period: parseInt(data.get("period") as string, 10),
      token_policies: removeEmptyStrings(String(data.get("policies")).split(",")),
      token_ttl: parseInt(data.get("initial_ttl") as string, 10),
    };
    const password = data.get("password") as string;
    if (password.length > 0) {
      apiData.password = password;
    }
    try {
      await this.props.api.createOrUpdateUserPassUser(baseMount, user, apiData);
      route(userPassUserViewURL(baseMount, user));
    } catch (e: unknown) {
      const error = e as Error;
      setErrorText(error.message);
    }
  }
}
