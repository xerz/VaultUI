import { AuthMethod } from "../../../../api/types/auth";
import { Component } from "preact";
import { DefaultPageProps } from "../../../../types/DefaultPageProps";
import { HeaderAndContent } from "../../../elements/HeaderAndContent";
import { PageTitle } from "../../../elements/PageTitle";
import { objectToMap, toStr } from "../../../../utils";
import i18next from "i18next";

export class AuthViewConfig extends Component<DefaultPageProps, { authMethod: AuthMethod }> {
  async componentDidMount() {
    const baseMount = this.props.matches["baseMount"];
    const authList = objectToMap(await this.props.api.listAuth()) as Map<string, AuthMethod>;
    const authMethod = authList.get(baseMount + "/");
    this.setState({ authMethod: authMethod });
  }
  render() {
    if (!this.state.authMethod) return;
    const baseMount = this.props.matches["baseMount"];
    const authMethod = this.state.authMethod;

    return (
      <>
        <PageTitle title={i18next.t("auth_view_config_title")} />
        <table class="uk-table">
          <tbody>
            <HeaderAndContent
              title={i18next.t("auth_view_config_type")}
              content={authMethod.type}
            />
            <HeaderAndContent title={i18next.t("auth_view_config_path")} content={baseMount} />
            <HeaderAndContent
              title={i18next.t("auth_view_config_description")}
              content={authMethod.description}
            />
            <HeaderAndContent
              title={i18next.t("auth_view_config_accessor")}
              content={authMethod.accessor}
            />
            <HeaderAndContent
              title={i18next.t("auth_view_config_local")}
              content={toStr(authMethod.local)}
            />
            <HeaderAndContent
              title={i18next.t("auth_view_config_seal_wrap")}
              content={toStr(authMethod.seal_wrap)}
            />
            <HeaderAndContent
              title={i18next.t("auth_view_config_list_when_unauth")}
              content={toStr(authMethod.config.listing_visibility)}
            />
            <HeaderAndContent
              title={i18next.t("auth_view_config_default_lease_ttl")}
              content={toStr(authMethod.config.default_lease_ttl)}
            />
            <HeaderAndContent
              title={i18next.t("auth_view_config_max_lease_ttl")}
              content={toStr(authMethod.config.max_lease_ttl)}
            />
            <HeaderAndContent
              title={i18next.t("auth_view_config_token_type")}
              content={toStr(authMethod.config.token_type)}
            />
          </tbody>
        </table>
      </>
    );
  }
}
