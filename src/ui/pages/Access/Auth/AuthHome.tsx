import { AuthMethod } from "../../../../api/types/auth";
import { Component, JSX } from "preact";
import { DefaultPageProps } from "../../../../types/DefaultPageProps";
import { PageTitle } from "../../../elements/PageTitle";
import { authViewConfigURL, userPassUserListURL } from "../../pageLinks";
import { notImplemented } from "../../../../pageUtils";
import { objectToMap } from "../../../../utils";
import { route } from "preact-router";
import i18next from "i18next";

export type AuthListElementProps = {
  path: string;
  method: AuthMethod;
};

export function AuthListElement(props: AuthListElementProps): JSX.Element {
  const isClickable = props.method.type != "token";

  const onHeaderLinkClick = async (props: AuthListElementProps) => {
    if (props.method.type == "userpass") {
      route(userPassUserListURL(props.path));
    }
  };

  return (
    <div class="uk-padding-small uk-background-secondary">
      {isClickable ? (
        <a class="uk-h4 uk-margin-bottom" onClick={() => onHeaderLinkClick(props)}>
          {props.path}
        </a>
      ) : (
        <span class="uk-h4 uk-margin-bottom">{props.path}</span>
      )}
      <span class="uk-text-muted">{` (${props.method.accessor})`}</span>
      <div class="uk-margin-top">
        <button
          class="uk-button uk-button-small uk-button-primary"
          onClick={async () => {
            route(authViewConfigURL(props.path));
          }}
        >
          {i18next.t("auth_home_view_config")}
        </button>
        <button class="uk-button uk-button-small uk-button-primary" onClick={notImplemented}>
          {i18next.t("auth_home_edit_config")}
        </button>
      </div>
    </div>
  );
}

export class AuthHome extends Component<DefaultPageProps, { authList: Map<string, AuthMethod> }> {
  async componentDidMount() {
    const authList = objectToMap(await this.props.api.listAuth()) as Map<string, AuthMethod>;
    this.setState({ authList });
  }
  render() {
    if (!this.state.authList) return;

    return (
      <>
        <PageTitle title={i18next.t("auth_home_title")} />
        <div>
          {Array.from(this.state.authList).map((values: [string, AuthMethod]) => (
            <AuthListElement path={values[0]} method={values[1]} />
          ))}
        </div>
      </>
    );
  }
}
