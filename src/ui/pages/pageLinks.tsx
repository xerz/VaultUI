// Delete Secret Engine

export function delSecretsEngineURL(baseMount: string): string {
  return `/secrets/delete_engine/${baseMount}`;
}

// Secrets / Key Value

export function kvNewURL(baseMount: string, secretPath?: string[]): string {
  return `/secrets/kv/new/${baseMount}` + (secretPath ? `/${secretPath.join("/")}` : "");
}

export function kvDeleteURL(baseMount: string, secretPath: string[], secret: string): string {
  return `/secrets/kv/delete/${secret}/${baseMount}/${secretPath.join("/")}`;
}

export function kvEditURL(baseMount: string, secretPath: string[], secret: string): string {
  return `/secrets/kv/edit/${secret}/${baseMount}/${secretPath.join("/")}`;
}

export function kvViewURL(baseMount: string, secretPath: string[], secret: string): string {
  return `/secrets/kv/view/${secret}/${baseMount}/${secretPath.join("/")}`;
}

export function kvListURL(baseMount: string, secretPath: string[]): string {
  console.log(baseMount, secretPath);
  return `/secrets/kv/list/${baseMount}/${secretPath.join("/")}`;
}

// Secrets / TOTP

export function totpNewURL(baseMount: string): string {
  return `/secrets/totp/new/${baseMount}`;
}

export function totpListURL(baseMount: string): string {
  return `/secrets/totp/list/${baseMount}`;
}

export function totpDeleteURL(baseMount: string, secret: string): string {
  return `/secrets/totp/delete/${baseMount}/${secret}`;
}

// Secrets / Transit

export function transitNewSecretURL(baseMount: string): string {
  return `/secrets/transit/new/${baseMount}`;
}

export function transitListSecretURL(baseMount: string, secret: string): string {
  return `/secrets/transit/list/${baseMount}/${secret}`;
}

export function transitViewSecretURL(baseMount: string, secret: string): string {
  return `/secrets/transit/view/${baseMount}/${secret}`;
}

export function transitEncryptSecretURL(baseMount: string, secret: string): string {
  return `/secrets/transit/encrypt/${baseMount}/${secret}`;
}

export function transitDecryptSecretURL(baseMount: string, secret: string): string {
  return `/secrets/transit/decrypt/${baseMount}/${secret}`;
}

export function transitRewrapSecretURL(baseMount: string, secret: string): string {
  return `/secrets/transit/rewrap/${baseMount}/${secret}`;
}

// Policies

export function policyNewURL(): string {
  return `/policies/new`;
}

export function policyViewURL(policyName: string): string {
  return `/policies/view/${policyName}`;
}

export function policyEditURL(policyName: string): string {
  return `/policies/edit/${policyName}`;
}

export function policyDeleteURL(policyName: string): string {
  return `/policies/delete/${policyName}`;
}

// Access / Auth

export function authViewConfigURL(baseMount: string): string {
  return `/access/auth/view/${baseMount}`;
}

// Access / Auth / UserPass

export function userPassUserListURL(baseMount: string): string {
  return `/access/auth/userpass/list/${baseMount}`;
}

export function userPassUserNewURL(baseMount: string): string {
  return `/access/auth/userpass/new/${baseMount}`;
}

export function userPassUserViewURL(baseMount: string, user: string): string {
  return `/access/auth/userpass/view/${baseMount}/${user}`;
}

export function userPassUserEditURL(baseMount: string, user: string): string {
  return `/access/auth/userpass/edit/${baseMount}/${user}`;
}

export function userPassUserDeleteURL(baseMount: string, user: string): string {
  return `/access/auth/userpass/delete/${baseMount}/${user}`;
}
