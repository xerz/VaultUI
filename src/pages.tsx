import { api } from "./globalAPI";
import { settings } from "./globalSettings";
import Router from "preact-router";

import { AccessHomePage } from "./ui/pages/Access/AccessHome";
import { AuthHome } from "./ui/pages/Access/Auth/AuthHome";
import { AuthViewConfig } from "./ui/pages/Access/Auth/AuthViewConfig";
import { DeleteSecretsEngine } from "./ui/pages/Secrets/DeleteSecretsEngine";
import { Home } from "./ui/pages/Home";
import { KeyValueDelete } from "./ui/pages/Secrets/KeyValue/KeyValueDelete";
import { KeyValueEdit } from "./ui/pages/Secrets/KeyValue/KeyValueEdit";
import { KeyValueList } from "./ui/pages/Secrets/KeyValue/KeyValueList";
import { KeyValueNew } from "./ui/pages/Secrets/KeyValue/KeyValueNew";
import { KeyValueView } from "./ui/pages/Secrets/KeyValue/KeyValueView";
import { Login } from "./ui/pages/Login";
import { Me } from "./ui/pages/Me";
import { NewKVEngine } from "./ui/pages/Secrets/NewEngines/NewKVEngine";
import { NewSecretsEngine } from "./ui/pages/Secrets/NewSecretsEngine";
import { NewTOTPEngine } from "./ui/pages/Secrets/NewEngines/NewTOTPEngine";
import { NewTransitEngine } from "./ui/pages/Secrets/NewEngines/NewTransitEngine";
import { PasswordGenerator } from "./ui/pages/PwGen";
import { PoliciesHome } from "./ui/pages/Policies/PoliciesHome";
import { PolicyDelete } from "./ui/pages/Policies/PolicyDelete";
import { PolicyEdit } from "./ui/pages/Policies/PolicyEdit";
import { PolicyNew } from "./ui/pages/Policies/PolicyNew";
import { PolicyView } from "./ui/pages/Policies/PolicyView";
import { Secrets } from "./ui/pages/Secrets/SecretsHome";
import { SetLanguage } from "./ui/pages/SetLanguage";
import { SetVaultURL } from "./ui/pages/SetVaultURL";
import { TOTPDelete } from "./ui/pages/Secrets/TOTP/TOTPDelete";
import { TOTPList } from "./ui/pages/Secrets/TOTP/TOTPList";
import { TOTPNew } from "./ui/pages/Secrets/TOTP/TOTPNew";
import { TransitDecrypt } from "./ui/pages/Secrets/Transit/TransitDecrypt";
import { TransitEncrypt } from "./ui/pages/Secrets/Transit/TransitEncrypt";
import { TransitList } from "./ui/pages/Secrets/Transit/TransitList";
import { TransitNew } from "./ui/pages/Secrets/Transit/TransitNew";
import { TransitRewrap } from "./ui/pages/Secrets/Transit/TransitRewrap";
import { TransitView } from "./ui/pages/Secrets/Transit/TransitView";
import { Unseal } from "./ui/pages/Unseal";
import { UserPassUserDelete } from "./ui/pages/Access/Auth/userpass/UserPassUserDelete";
import { UserPassUserEdit } from "./ui/pages/Access/Auth/userpass/UserPassUserEdit";
import { UserPassUserNew } from "./ui/pages/Access/Auth/userpass/UserPassUserNew";
import { UserPassUserView } from "./ui/pages/Access/Auth/userpass/UserPassUserView";
import { UserPassUsersList } from "./ui/pages/Access/Auth/userpass/UserPassUsersList";

export const Main = () => (
  <Router>
    <Home path="/" settings={settings} api={api} />
    <Me path="/me" settings={settings} api={api} />
    <Login path="/login" settings={settings} api={api} />
    <PasswordGenerator path="/pw_gen" settings={settings} api={api} />
    <SetVaultURL path="/set_vault_url" settings={settings} api={api} />
    <Unseal path="/unseal" settings={settings} api={api} />
    <SetLanguage path="/set_language" settings={settings} api={api} />

    <Secrets path="/secrets" settings={settings} api={api} />
    <DeleteSecretsEngine path="/secrets/delete_engine/:mount" settings={settings} api={api} />

    <NewSecretsEngine path="/secrets/new_secrets_engine" settings={settings} api={api} />
    <NewKVEngine path="/secrets/new_secrets_engine/kv" settings={settings} api={api} />
    <NewTOTPEngine path="/secrets/new_secrets_engine/totp" settings={settings} api={api} />
    <NewTransitEngine path="/secrets/new_secrets_engine/trasit" settings={settings} api={api} />

    <KeyValueNew path="/secrets/kv/new/:baseMount/:secretPath*?" settings={settings} api={api} />
    <KeyValueList path="/secrets/kv/list/:baseMount/:secretPath*?" settings={settings} api={api} />
    <KeyValueView
      path="/secrets/kv/view/:item/:baseMount/:secretPath*?"
      settings={settings}
      api={api}
    />
    <KeyValueEdit
      path="/secrets/kv/edit/:item/:baseMount/:secretPath*?"
      settings={settings}
      api={api}
    />
    <KeyValueDelete
      path="/secrets/kv/delete/:item/:baseMount/:secretPath*?"
      settings={settings}
      api={api}
    />

    <TOTPList path="/secrets/totp/list/:baseMount" settings={settings} api={api} />
    <TOTPNew path="/secrets/totp/new/:baseMount" settings={settings} api={api} />
    <TOTPDelete path="/secrets/totp/delete/:baseMount/:item" settings={settings} api={api} />

    <TransitNew path="/secrets/transit/new/:baseMount" settings={settings} api={api} />
    <TransitList path="/secrets/transit/list/:baseMount" settings={settings} api={api} />
    <TransitView
      path="/secrets/transit/view/:baseMount/:secretItem"
      settings={settings}
      api={api}
    />
    <TransitEncrypt
      path="/secrets/transit/encrypt/:baseMount/:secretItem"
      settings={settings}
      api={api}
    />
    <TransitDecrypt
      path="/secrets/transit/decrypt/:baseMount/:secretItem"
      settings={settings}
      api={api}
    />
    <TransitRewrap
      path="/secrets/transit/rewrap/:baseMount/:secretItem"
      settings={settings}
      api={api}
    />

    <PoliciesHome path="/policies" settings={settings} api={api} />
    <PolicyNew path="/policies/new" settings={settings} api={api} />
    <PolicyView path="/policies/view/:policyName" settings={settings} api={api} />
    <PolicyEdit path="/policies/edit/:policyName" settings={settings} api={api} />
    <PolicyDelete path="/policies/delete/:policyName" settings={settings} api={api} />

    <AccessHomePage path="/access" settings={settings} api={api} />
    <AuthHome path="/access/auth" settings={settings} api={api} />
    <AuthViewConfig path="/access/auth/view/:baseMount" settings={settings} api={api} />

    <UserPassUsersList path="/access/auth/userpass/list/:baseMount" settings={settings} api={api} />
    <UserPassUserNew path="/access/auth/userpass/new/:baseMount" settings={settings} api={api} />
    <UserPassUserView
      path="/access/auth/userpass/view/:baseMount/:user"
      settings={settings}
      api={api}
    />
    <UserPassUserEdit
      path="/access/auth/userpass/edit/:baseMount/:user"
      settings={settings}
      api={api}
    />
    <UserPassUserDelete
      path="/access/auth/userpass/delete/:baseMount/:user"
      settings={settings}
      api={api}
    />

    <div default>
      <p>PAGE NOT YET IMPLEMENTED</p>
    </div>
  </Router>
);
