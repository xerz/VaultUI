import { API } from "./api/API";
import { settings } from "./globalSettings";

export const api = new API(settings);
