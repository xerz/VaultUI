"use strict";

// JS & CSS

/* eslint-disable */
import "./scss/main.scss";
import UIkit from "uikit";
// Don't Sort These!
import Icons from "uikit/dist/js/uikit-icons";
// @ts-ignore
UIkit.use(Icons);

import Prism from "prismjs";
// Don't Sort These!
import "prismjs/components/prism-hcl";
import "prismjs/components/prism-json";

Prism.highlightAll();
/* eslint-enable */

// @ts-ignore
import translations from "./translations/index.mjs";

// Actual Imports
import { formatDistance } from "./formatDistance";
//import { pageList } from "./allPages";
import { Main } from "./pages";
import { NavBar } from "./ui/elements/NavBar";
import { api } from "./globalAPI";
import { getCurrentUrl, route } from "preact-router";
import { playground } from "./playground";
import { render } from "preact";
import { settings } from "./globalSettings";
import i18next from "i18next";

async function onLoad(): Promise<void> {
  document.documentElement.dir = settings.pageDirection;

  render(
    <>
      <NavBar />
      <div class="uk-container uk-container-medium uk-align-center">
        <div class="uk-card uk-card-body">
          <Main />
        </div>
      </div>
    </>,
    document.body,
  );

  if (process.env.NODE_ENV == "development") {
    await playground();
  }

  //await pageRouter.changePage(pageState.currentPage);

  setInterval(async () => {
    console.log(getCurrentUrl());
    if (getCurrentUrl() != "/unseal") {
      // TODO: check if api is accessable, not if its not set
      if (settings.apiURL.length == 0) {
        return;
      }
      const sealStatus = await api.getSealStatus();
      if (sealStatus.sealed) {
        route("/unseal", false);
        return;
      }
    }
  }, 5000);
}

document.addEventListener(
  "DOMContentLoaded",
  async () => {
    console.log("Loading...");
    // @ts-expect-error
    console.log("Build Data:", BUILD_STRING);
    await i18next.init({
      lng: settings.language,
      fallbackLng: "en",
      debug: true,
      // @ts-ignore
      resources: Object.fromEntries(
        Object.entries(translations).map(([k, v]) => [k, { translation: v }]),
      ),
      interpolation: {
        escape: (str) => {
          return str;
        },
        format: function (value: unknown, format, _): string {
          if (format === "until_date" && value instanceof Date)
            return formatDistance(new Date(), new Date(value), settings.language);
          return value as string;
        },
      },
    });
    await onLoad();
  },
  false,
);
