//import { PageType } from "./pagerouter/PageType";
//
//import { AccessHomePage } from "./ui/pages/Access/AccessHome";
//import { AuthHomePage } from "./ui/pages/Access/Auth/AuthHome";
//import { AuthViewConfigPage } from "./ui/pages/Access/Auth/AuthViewConfig";
//import { DeleteSecretsEnginePage } from "./ui/pages/Secrets/DeleteSecretsEngine";
//import { HomePage } from "./ui/pages/Home";
//import { KeyValueDeletePage } from "./ui/pages/Secrets/KeyValue/KeyValueDelete";
//import { KeyValueNewPage } from "./ui/pages/Secrets/KeyValue/KeyValueNew";
//import { KeyValueSecretEditPage } from "./ui/pages/Secrets/KeyValue/KeyValueSecretsEdit";
//import { KeyValueSecretPage } from "./ui/pages/Secrets/KeyValue/KeyValueSecret";
//import { KeyValueVersionsPage } from "./ui/pages/Secrets/KeyValue/KeyValueVersions";
//import { KeyValueViewPage } from "./ui/pages/Secrets/KeyValue/KeyValueView";
//import { LoginPage } from "./ui/pages/Login";
//import { MePage } from "./ui/pages/Me";
//import { NewKVEnginePage } from "./ui/pages/Secrets/NewEngines/NewKVEngine";
//import { NewSecretsEnginePage } from "./ui/pages/Secrets/NewSecretsEngine";
//import { NewTOTPEnginePage } from "./ui/pages/Secrets/NewEngines/NewTOTPEngine";
//import { NewTransitEnginePage } from "./ui/pages/Secrets/NewEngines/NewTransitEngine";
//import { NewTransitKeyPage } from "./ui/pages/Secrets/Transit/NewTransitKey";
//import { Page } from "./types/Page";
//import { PoliciesHomePage } from "./ui/pages/Policies/PoliciesHome";
//import { PolicyDeletePage } from "./ui/pages/Policies/PolicyDelete";
//import { PolicyEditPage } from "./ui/pages/Policies/PolicyEdit";
//import { PolicyNewPage } from "./ui/pages/Policies/PolicyNew";
//import { PolicyViewPage } from "./ui/pages/Policies/PolicyView";
//import { PwGenPage } from "./ui/pages/PwGen";
//import { SecretsHomePage } from "./ui/pages/Secrets/SecretsHome";
//import { SetLanguagePage } from "./ui/pages/SetLanguage";
//import { SetVaultURLPage } from "./ui/pages/SetVaultURL";
//import { TOTPDeletePage } from "./ui/pages/Secrets/TOTP/TOTPDelete";
//import { TOTPNewPage } from "./ui/pages/Secrets/TOTP/TOTPNew";
//import { TOTPViewPage } from "./ui/pages/Secrets/TOTP/TOTPView";
//import { TransitDecryptPage } from "./ui/pages/Secrets/Transit/TransitDecrypt";
//import { TransitEncryptPage } from "./ui/pages/Secrets/Transit/TransitEncrypt";
//import { TransitRewrapPage } from "./ui/pages/Secrets/Transit/TransitRewrap";
//import { TransitViewPage } from "./ui/pages/Secrets/Transit/TransitView";
//import { TransitViewSecretPage } from "./ui/pages/Secrets/Transit/TransitViewSecret";
//import { UnsealPage } from "./ui/pages/Unseal";
//import { UserPassUserDeletePage } from "./ui/pages/Access/Auth/userpass/UserPassUserDelete";
//import { UserPassUserEditPage } from "./ui/pages/Access/Auth/userpass/UserPassUserEdit";
//import { UserPassUserNewPage } from "./ui/pages/Access/Auth/userpass/UserPassUserNew";
//import { UserPassUserViewPage } from "./ui/pages/Access/Auth/userpass/UserPassUserView";
//import { UserPassUsersListPage } from "./ui/pages/Access/Auth/userpass/UserPassUsersList";
//import { getObjectKeys } from "./utils";
//
//type pagesList = {
//  [key: string]: Page;
//};
//
//export const allPages: pagesList = {
//  HOME: new HomePage(),
//  LOGIN: new LoginPage(),
//  SET_VAULT_URL: new SetVaultURLPage(),
//  UNSEAL: new UnsealPage(),
//  SET_LANGUAGE: new SetLanguagePage(),
//  ME: new MePage(),
//  PW_GEN: new PwGenPage(),
//
//  POLICIES_HOME: new PoliciesHomePage(),
//  POLICY_VIEW: new PolicyViewPage(),
//  POLICY_NEW: new PolicyNewPage(),
//  POLICY_EDIT: new PolicyEditPage(),
//  POLICY_DELETE: new PolicyDeletePage(),
//
//  ACCESS_HOME: new AccessHomePage(),
//
//  AUTH_HOME: new AuthHomePage(),
//  AUTH_VIEW_CONFIG: new AuthViewConfigPage(),
//
//  USERPASS_USERS_LIST: new UserPassUsersListPage(),
//  USERPASS_USER_VIEW: new UserPassUserViewPage(),
//  USERPASS_USER_EDIT: new UserPassUserEditPage(),
//  USERPASS_USER_NEW: new UserPassUserNewPage(),
//  USERPASS_USER_DELETE: new UserPassUserDeletePage(),
//
//  SECRETS_HOME: new SecretsHomePage(),
//
//  TOTP_VIEW: new TOTPViewPage(),
//  TOTP_NEW: new TOTPNewPage(),
//  TOTP_DELETE: new TOTPDeletePage(),
//
//  TRANSIT_VIEW: new TransitViewPage(),
//  TRANSIT_NEW_KEY: new NewTransitKeyPage(),
//  TRANSIT_VIEW_SECRET: new TransitViewSecretPage(),
//  TRANSIT_ENCRYPT: new TransitEncryptPage(),
//  TRANSIT_DECRYPT: new TransitDecryptPage(),
//  TRANSIT_REWRAP: new TransitRewrapPage(),
//
//  KEY_VALUE_VIEW: new KeyValueViewPage(),
//  KEY_VALUE_SECRET: new KeyValueSecretPage(),
//  KEY_VALUE_VERSIONS: new KeyValueVersionsPage(),
//  KEY_VALUE_NEW_SECRET: new KeyValueNewPage(),
//  KEY_VALUE_DELETE: new KeyValueDeletePage(),
//  KEY_VALUE_SECRET_EDIT: new KeyValueSecretEditPage(),
//
//  DELETE_SECRET_ENGINE: new DeleteSecretsEnginePage(),
//
//  NEW_SECRETS_ENGINE: new NewSecretsEnginePage(),
//  NEW_KV_ENGINE: new NewKVEnginePage(),
//  NEW_TOTP_ENGINE: new NewTOTPEnginePage(),
//  NEW_TRANSIT_ENGINE: new NewTransitEnginePage(),
//};
//
//// This should implement all o PageListType
//class PageList {
//  constructor(pages: pagesList) {
//    this.pages = pages;
//  }
//
//  private pages: pagesList;
//
//  async getPageIDs(): Promise<string[]> {
//    return getObjectKeys(this.pages);
//  }
//  async getPage(pageID: string): Promise<PageType> {
//    return this.pages[pageID];
//  }
//}
//
//export const pageList = new PageList(allPages);
