#!/usr/bin/env bash

npx eslint --cache -c .eslintrc.json "$@" --ext .js,.ts,.tsx